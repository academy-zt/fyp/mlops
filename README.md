# MLOps

[![pipeline status](https://jihulab.com/academy/fyp/mlops/badges/main/pipeline.svg)](https://jihulab.com/academy/fyp/mlops/-/commits/main)[![Latest Release](https://jihulab.com/academy/fyp/mlops/-/badges/release.svg)](https://jihulab.com/academy/fyp/mlops/-/releases)

## Background

Recently, MLOps is becoming more and more popular. TBD

## Structure of the whole system

![structure](img/structure.png)

### Pgmlflow

Pgmlflow is a Python package responsible for triggering the Gomlflow. When a MR pipeline (new training) is activated, Pgmlflow will send a POST request to Gomlflow. The body of the request includes the MR ID, MLflow run ID and the project ID on Jihu GitLab.

### Gomlflow

Gomlflow is a web server built by Golang. The server will receive requests from Pgmlflow. When a new request is received, Gomlflow will send a GET request to the MLflow API endpoint to query the result of the run (i.e. the ML training) with the run ID. Then it will send a POST request to Jihu GitLab and return the result of the run to the merge request.

## Scenario to understand the workflow
### Update the dataset in the repository
#### 1. Create an issue and a Merge Request
When you want to update any material in the repository, firstly, you should create an issue and a Merge Request. The MR will create a new branch, where you will modify the material.

#### 2. Update the dataset in the new branch and commit it
The second step is to commit your updates to the branch. A CI/CD pipeline will be triggered based on the .gitlab-ci.yml, the CI/CD configuration file, to train the new model with MLflow. A training process in MLflow is called a Run.

#### 3. Pgmlflow will send the MR ID and Run ID to Gomlflow
When the run is finished, Pgmlflow will collect the run ID and MR ID and send put them in a POST request forwarding to Gomlflow.

#### 4. Gomlflow will query the training result and comment on the MR
When the POST request from Pgmlflow is received, Gomlfow will query the run result according to the run ID. Then, Gomlflow will return the result to the MR comment area. To achieve that, Gomlflow will send a GET request to MLflow and a POST request to Jihu GitLab.

#### 5. Accept the update and merge the code
If we accept the update and choose to merge the code, then the code will be merged into the default branch (main, master, etc.).

#### 6. A new CI/CD pipeline will be activated in the default branch
When the update is merged into the default branch, a new CI/CD pipeline will be triggered. In this stage, the pipeline will build the model and trigger the Pgmlflow again. However, this time Pgmlflow will not send any request to Gomlflow. Instead, Pgmlflow will store the model artifacts in local and the pipeline will send the artifacts to following jobs.

#### 7. Upload the model artifacts to Jihu GitLab package registry
In this stage, the pipeline will collect the artifacts built by previous jobs and package them into a tar package. Then, the package will be sent to Jihu GitLab.

#### 8. Launch a new release
In this final stage, the pipeline will launch a new release in Jihu GitLab. The release includes all the source code and the model artifacts, and this is the end of the whole workflow.


![clarity](https://www.clarity.ms/tag/gx5lutgxoq)
